package com.gitlab.edufuga.wordlines.sentencereader;

import com.gitlab.edufuga.wordlines.sentencesreader.SentencesReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SentenceReader {
   private final ArrayDeque<String> sentencesStack = new ArrayDeque<>();
   private final SentencesReader sentencesReader;

   public SentenceReader(SentencesReader sentencesReader) {
      this.sentencesReader = sentencesReader;
   }

   public String getSentenceForWord(String word) throws IOException {
      if (sentencesStack.isEmpty()) {
         List<String> sentences = sentencesReader.getSentencesForWord(word);
         if (sentences.isEmpty()) {
            return "";
         }

         Random random = new Random(System.currentTimeMillis());
         Collections.shuffle(sentences, random);

         sentencesStack.addAll(sentences);
      }

      return sentencesStack.pop();
   }

   public static void main(String[] args) throws IOException {
      if (args.length != 2) {
         System.out.println("Give me the file path and the word to read the sentences for.");
         return;
      }

      String filePathString = args[0];
      if (filePathString == null || filePathString.isEmpty()) {
         System.out.println("The file path isn't given.");
         return;
      }

      String word = args[1];
      if (word == null || word.isEmpty()) {
         System.out.println("The word isn't given.");
         return;
      }

      Path filePath = Paths.get(filePathString);

      if (Files.notExists(filePath)) {
         System.out.println("The file path doesn't exist.");
         return;
      }

      SentencesReader sentencesReader = new SentencesReader(filePath.toFile());
      SentenceReader sentenceReader = new SentenceReader(sentencesReader);
      String sentence = sentenceReader.getSentenceForWord(word);
      System.out.println(sentence);
   }
}
